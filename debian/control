Source: gnupg2
Section: utils
Priority: optional
Maintainer: Debian GnuPG Maintainers <pkg-gnupg-maint@lists.alioth.debian.org>
Uploaders:
 Eric Dorland <eric@debian.org>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
Standards-Version: 3.9.8
Build-Depends:
 automake,
 autopoint,
 debhelper (>= 9),
 dh-autoreconf,
 file,
 gettext,
 ghostscript,
 imagemagick,
 libassuan-dev (>= 2.4.3),
 libbz2-dev,
 libcurl4-gnutls-dev,
 libgcrypt20-dev (>= 1.7.0),
 libgnutls28-dev (>= 3.0),
 libgpg-error-dev (>= 1.26-2~),
 libksba-dev (>= 1.3.4),
 libldap2-dev,
 libnpth0-dev (>= 1.2),
 libreadline-dev,
 librsvg2-bin,
 libsqlite3-dev,
 libusb-1.0-0-dev [!hurd-any],
 pkg-config,
 texinfo,
 transfig,
 zlib1g-dev | libz-dev,
Build-Depends-Indep:
 binutils-multiarch [!amd64 !i386],
Vcs-Git: https://anonscm.debian.org/git/pkg-gnupg/gnupg2.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-gnupg/gnupg2.git
Homepage: https://www.gnupg.org/

Package: gnupg-agent
Architecture: any
Multi-Arch: foreign
Depends:
 pinentry-curses | pinentry,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gnupg | gnupg2 (= ${binary:Version}) | gpgsm,
Suggests:
 dbus-user-session,
 libpam-systemd,
 pinentry-gnome3,
 scdaemon,
Provides:
 gpg-agent,
Description: GNU privacy guard - cryptographic agent
 GnuPG is GNU's tool for secure communication and data storage.
 It can be used to encrypt data and to create digital signatures.
 It includes an advanced key management facility and is compliant
 with the proposed OpenPGP Internet standard as described in RFC4880.
 .
 This package contains the agent program gpg-agent which handles all
 secret key material for OpenPGP and S/MIME use.  The agent also
 provides a passphrase cache, which is used by pre-2.1 versions of
 GnuPG for OpenPGP operations.

Package: gpg-wks-server
Architecture: any
Multi-Arch: foreign
Depends:
 gnupg2 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: GNU privacy guard - Web Key Service server
 GnuPG is GNU's tool for secure communication and data storage.
 It can be used to encrypt data and to create digital signatures.
 It includes an advanced key management facility and is compliant
 with the proposed OpenPGP Internet standard as described in RFC4880.
 .
 This package provides the GnuPG server for the Web Key Service
 protocol.
 .
 A Web Key Service is a service that allows users to upload keys per
 mail to be verified over https as described in
 https://tools.ietf.org/html/draft-koch-openpgp-webkey-service
 .
 For more information see: https://wiki.gnupg.org/WKS

Package: gpg-wks-client
Architecture: any
Multi-Arch: foreign
Depends:
 dirmngr (= ${binary:Version}),
 gnupg2 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: GNU privacy guard - Web Key Service client
 GnuPG is GNU's tool for secure communication and data storage.
 It can be used to encrypt data and to create digital signatures.
 It includes an advanced key management facility and is compliant
 with the proposed OpenPGP Internet standard as described in RFC4880.
 .
 This package provides the GnuPG client for the Web Key Service
 protocol.
 .
 A Web Key Service is a service that allows users to upload keys per
 mail to be verified over https as described in
 https://tools.ietf.org/html/draft-koch-openpgp-webkey-service
 .
 For more information see: https://wiki.gnupg.org/WKS

Package: scdaemon
Architecture: any
Multi-Arch: foreign
Depends:
 gnupg-agent (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Enhances:
 gnupg-agent,
Description: GNU privacy guard - smart card support
 GnuPG is GNU's tool for secure communication and data storage.
 It can be used to encrypt data and to create digital signatures.
 It includes an advanced key management facility and is compliant
 with the proposed OpenPGP Internet standard as described in RFC4880.
 .
 This package contains the smart card program scdaemon, which is used
 by gnupg-agent to access OpenPGP smart cards.

Package: gpgsm
Architecture: any
Multi-Arch: foreign
Depends:
 gnupg-agent (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 dirmngr (= ${binary:Version}),
Breaks:
 gnupg2 (<< 2.1.10-2),
Replaces:
 gnupg2 (<< 2.1.10-2),
Description: GNU privacy guard - S/MIME version
 GnuPG is GNU's tool for secure communication and data storage.
 It can be used to encrypt data and to create digital signatures.
 It includes an advanced key management facility and is compliant
 with the proposed OpenPGP Internet standard as described in RFC4880.
 .
 This package contains the gpgsm program. gpgsm is a tool to provide
 digital encryption and signing services on X.509 certificates and the
 CMS protocol. gpgsm includes complete certificate management.

Package: gnupg2
Architecture: any
Multi-Arch: foreign
Depends:
 gnupg-agent (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 dirmngr (= ${binary:Version}),
 gnupg2-l10n (= ${source:Version}),
 ${shlibs:Recommends},
Suggests:
 parcimonie,
 xloadimage,
Conflicts:
 gpg-idea (<= 2.2)
Breaks:
 dirmngr (<< 2.1.0~)
Description: GNU privacy guard - a free PGP replacement (new v2.x)
 GnuPG is GNU's tool for secure communication and data storage.
 It can be used to encrypt data and to create digital signatures.
 It includes an advanced key management facility and is compliant
 with the proposed OpenPGP Internet standard as described in RFC4880.
 .
 This package contains /usr/bin/gpg2 and some helper utilities like
 gpgconf and kbxutil.

Package: gpgv2
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 gnupg2 (<< 2.0.21-2),
Replaces:
 gnupg2 (<< 2.0.21-2),
Suggests:
 gnupg2,
Description: GNU privacy guard - signature verification tool (new v2.x)
 GnuPG is GNU's tool for secure communication and data storage.
 .
 gpgv2 is actually a stripped-down version of gpg2 which is only able
 to check signatures. It is somewhat smaller than the fully-blown gpg2
 and uses a different (and simpler) way to check that the public keys
 used to make the signature are valid. There are no configuration
 files and only a few options are implemented.

Package: dirmngr
Architecture: any
Depends:
 adduser,
 lsb-base (>= 3.2-13),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gnupg2 (= ${binary:Version}),
 ${shlibs:Recommends},
Enhances:
 gnupg2,
 gpgsm,
 squid,
Breaks:
 gnupg2 (<< 2.1.10-2),
Replaces:
 gnupg2 (<< 2.1.10-2),
Suggests:
 dbus-user-session,
 libpam-systemd,
 pinentry-gnome3,
 tor,
Description: GNU privacy guard - network certificate management service
 dirmngr is a server for managing and downloading OpenPGP and X.509
 certificates, as well as updates and status signals related to those
 certificates.  For OpenPGP, this means pulling from the public
 HKP/HKPS keyservers, or from LDAP servers.  For X.509 this includes
 Certificate Revocation Lists (CRLs) and Online Certificate Status
 Protocol updates (OCSP).  It is capable of using tor for network
 access.
 .
 dirmngr is used for network access by gpg, gpgsm, and dirmngr-client,
 among other tools.

Package: gnupg2-l10n
Architecture: all
Priority: extra
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Enhances:
 gnupg2,
Breaks:
 gnupg2 (<< 2.1.14-2~),
Replaces:
 gnupg2 (<< 2.1.14-2~),
Description: GNU privacy guard - localization files
 GnuPG is GNU's tool for secure communication and data storage.
 It can be used to encrypt data and to create digital signatures.
 It includes an advanced key management facility and is compliant
 with the proposed OpenPGP Internet standard as described in RFC 4880.
 .
 This package contains the translation files for the use of GnuPG in
 non-English locales.
